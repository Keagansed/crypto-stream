import { WebSocketGateway, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Subject, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CryptoCompareCoinbaseSimplified } from 'src/interfaces/crypto-compare-coinbase-simplified.interface';
import { CryptoCompareCoinbase } from 'src/interfaces/crypto-compare-coinbase.interface';
import * as WebSocket from "ws";

@WebSocketGateway()
export class CryptoGateway implements OnGatewayConnection, OnGatewayDisconnect {
    // Subscribe to websocket using API Key from environment variables
    private ws = new WebSocket("wss://streamer.cryptocompare.com/v2?api_key=" + environment.APIKey)

    private static crypto$: Subject<any> = undefined
    private subscriptions: Array<{fromCurr: string, toCurr: string, subscription: Subscription}> = []
    private subjectSubscriptions: Array<Subscription> = []

    constructor() {
        // Start looking for new messages
        this.onMessage()
    }

    @WebSocketServer() server;

    async handleConnection(){
    }

    async handleDisconnect(){
        // Remove all subscriptions when a client disconnects
        this.removeAllSubscriptions()
    }

    async emit(subcriptionString: string, data: any) {
        // Emit changes to the exchange rate using the given subscription string
        this.server.emit(subcriptionString, data)
    }

    // Subscribe to the websocket
    subscribe(fromCurr: string, toCurr: string) {
        let found = false
        this.subscriptions.map((subscription) => {
            if (subscription.fromCurr === fromCurr && subscription.toCurr === toCurr)
                found = true
        })
        if (!found) {
            var subRequest = {
                "action": "SubAdd",
                "subs": ["2~Coinbase~" + fromCurr + "~" + toCurr]
            };
            this.ws.send(JSON.stringify(subRequest))
        }
        let subj = this.getData().subscribe((data: string) => {
            let coinbase: CryptoCompareCoinbase = JSON.parse(data)

              if ((coinbase.FROMSYMBOL !== undefined && coinbase.TOSYMBOL !== undefined) && coinbase.FROMSYMBOL === fromCurr && coinbase.TOSYMBOL === toCurr) {
                let coinbaseSimplified: CryptoCompareCoinbaseSimplified = {
                    FROMSYMBOL: coinbase.FROMSYMBOL,
                    TOSYMBOL: coinbase.TOSYMBOL,
                    PRICE: coinbase.PRICE
                } 
                this.emit('crypto-changes-' + fromCurr + '-' + toCurr, JSON.stringify(coinbaseSimplified))
              }
            })
        // Add new subscription to array so that we can subscribe and unsubscribe as necessary
        this.subscriptions.push({fromCurr: fromCurr, toCurr: toCurr, subscription: subj})
    }

    // Unsubscribe from websocket
    unsubscribe(fromCurr: string, toCurr: string) {
        var subRequest = {
            "action": "SubRemove",
            "subs": ["2~Coinbase~" + fromCurr + "~" + toCurr]
        };
        this.ws.send(JSON.stringify(subRequest))
        this.subscriptions.filter((subscription) => {return subscription.fromCurr === fromCurr && subscription.toCurr === toCurr}).forEach((subscription) => {
            // Unsibscribe from subscription
            subscription.subscription.unsubscribe()
        })
        // Filter out subscriptions that are being unsubscribed from
        this.subscriptions = this.subscriptions.filter((subscription) => !(subscription.fromCurr == fromCurr && subscription.toCurr == toCurr))
    }

    // This method is responsible for updatin the crypto$ subject every time a new message comes from the websocket
    onMessage() {
        this.ws.on("message", (message) =>  {
            if (CryptoGateway.crypto$ === undefined) {
                CryptoGateway.crypto$ = new Subject()
            }
            CryptoGateway.crypto$.next(message)
        })
    }

    // This method returns the crypto$ subject
    getData(): Subject<any> {
        return CryptoGateway.crypto$
    }

    // Call this method to unsubscribe from all subscriptions when a client disconnects
    removeAllSubscriptions(): void {
        this.subscriptions.forEach((subscription) => {
            this.unsubscribe(subscription.fromCurr, subscription.toCurr)
        })
        this.unsubscribeAll()
    }

    // Unsubscribes from all subject subscriptions
    unsubscribeAll() {
        this.subjectSubscriptions.forEach((subscription) => {
          subscription.unsubscribe()
        })
      }

}
