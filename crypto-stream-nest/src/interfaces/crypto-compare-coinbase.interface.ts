export interface CryptoCompareCoinbase {
    TYPE?:string,
    MARKET?:string,
    FROMSYMBOL?:string,
    TOSYMBOL?:string,
    FLAGS?:1,
    PRICE?:number,
    LASTUPDATE?:number,
    LASTVOLUME?:number,
    LASTVOLUMETO?:number,
    LASTTRADEID?:string,
    VOLUMEDAY?:number,
    VOLUMEDAYTO?:number,
    VOLUME24HOUR?:number,
    VOLUME24HOURTO?:number,
    VOLUMEHOUR?:number,
    VOLUMEHOURTO?:number
}
