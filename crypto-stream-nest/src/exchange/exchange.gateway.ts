import { Controller, Get, Post, Query } from '@nestjs/common';
import { CryptoGateway } from 'src/crypto/crypto.gateway';
import { SubRequest } from 'src/interfaces/sub-request.interface';

@Controller()
export class ExchangeGateway {
  constructor(private cryptoService: CryptoGateway) {}

  // This is exposed to client and allows them to subscribe to an currency pair
  @Get('subExchange') 
  subExchange(@Query() query: SubRequest): string{
      this.cryptoService.subscribe(query.fromCurr,query.toCurr)
      return 'success'
  }

  // This is exposed to client and allows them to unsubscribe to an currency pair
  @Get('unsubExchange') 
  unsubExchange(@Query() query: SubRequest): string{
      this.cryptoService.unsubscribe(query.fromCurr,query.toCurr)
      return 'success'
  }

}
