import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CryptoGateway } from './crypto/crypto.gateway';
import { ExchangeGateway } from './exchange/exchange.gateway';
// app.module.ts

@Module({
  imports: [],
  controllers: [AppController, ExchangeGateway],
  providers: [AppService, CryptoGateway, ExchangeGateway]
})
export class AppModule {}
