# Crypto Stream

Crypto Stream is a minimalistic app used to track and compare exchange rates of [Crypto Currencies](https://en.wikipedia.org/wiki/Cryptocurrency)

## Installation

Open a new terminal window and go into the 'crypto-stream-nest' directory:
```bash
cd crypto-stream-nest
```
Install the node modules:
```bash
npm install
```
Start the NestJS DEV server:
```bash
npm run start:dev
```

Open a second terminal window and go into the 'crypto-stream-angular' directory:
```bash
cd crypto-stream-angular
```
Install the node modules:
```bash
npm install
```
Start the Angular DEV server:
```bash
npm run start
```
Open a new browser window and navigate to [http://localhost:4200/home](http://localhost:4200/home)

Simply click on the plus button to add new cards to track exchange rates. Similarly, click on the cross at the top right of each card to remove them.

## Testing
Run the following command in the 'crypto-stream-angular' directory (ensuring that both the NestJS and Angular servers are running):
```bash
npx cypress open
```
When the Cypress window opens, you can run the tests by clicking on the relevant test and making sure all subtests succeed.

Please make sure to update tests as appropriate.
