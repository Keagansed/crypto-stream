describe("Basic functionality tests", () => {
    it("should visit home page", () => {
      cy.visit("http://localhost:4200/home");
    });
    it("should redirect to home page", () => {
        cy.visit("http://localhost:4200/wrgkrbvkjenbjl");
        cy.url().should('include', '/home')
      });
    it("should add BTC to USD tracker", () => {
        cy.get('mat-icon').click();
        cy.get('select.fromCurr').select('BTC');
        cy.get('select.toCurr').select('USD');
        cy.contains('Add').click();
        cy.contains('BTC')
        cy.contains('USD')
      });
      it("should remove BTC to USD tracker", () => {
        cy.get('.remove-button').click();
        cy.contains('BTC').should('not.exist');
        cy.contains('USD').should('not.exist');
      });
      it("should add ETH to GBP tracker", () => {
        cy.get('mat-icon').click();
        cy.get('select.fromCurr').select('ETH');
        cy.get('select.toCurr').select('GBP');
        cy.contains('Add').click();
        cy.contains('ETH')
        cy.contains('GBP')
      });
      it("should remove ETH to GBP tracker", () => {
        cy.get('.remove-button').click();
        cy.contains('ETH').should('not.exist');
        cy.contains('GBP').should('not.exist');
      });
      it("should add XRP to EUR tracker", () => {
        cy.get('mat-icon').click();
        cy.get('select.fromCurr').select('XRP');
        cy.get('select.toCurr').select('EUR');
        cy.contains('Add').click();
        cy.contains('XRP')
        cy.contains('EUR')
      });
      it("should remove XRP to EUR tracker", () => {
        cy.get('.remove-button').click();
        cy.contains('XRP').should('not.exist');
        cy.contains('EUR').should('not.exist');
      });
      it("should not add BTC to ZAR tracker", () => {
        cy.get('mat-icon').click();
        cy.get('select.fromCurr').select('BTC');
        cy.get('select.toCurr').select('ZAR');
        cy.contains('Add').click();
        cy.contains('BTC').should('not.exist');
        cy.contains('ZAR').should('not.exist');
      });
      it("should not add BTC to JPY tracker", () => {
        cy.get('mat-icon').click();
        cy.get('select.fromCurr').select('BTC');
        cy.get('select.toCurr').select('JPY');
        cy.contains('Add').click();
        cy.contains('BTC').should('not.exist');
        cy.contains('JPY').should('not.exist');
      });
  });