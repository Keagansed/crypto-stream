import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor(private socket: Socket, private httpClient: HttpClient) { 
  }

  // Make call to NestJS to subscribe to currency pair
  subscribe(fromCurr: string, toCurr: string) {
    this.httpClient.get('http://localhost:3000/subExchange', {
      params: {
        fromCurr: fromCurr,
        toCurr: toCurr
      },
      observe: 'response'
    })
    .toPromise()
    .then(response => {
    })
    .catch(console.log);
  }

  // Make call to NestJS to unsubscribe to currency pair
  unsubscribe(fromCurr: string, toCurr: string) {
    this.httpClient.get('http://localhost:3000/unsubExchange', {
      params: {
        fromCurr: fromCurr,
        toCurr: toCurr
      },
      observe: 'response'
    })
    .toPromise()
    .then(response => {
    })
    .catch(console.log);
  }

  // Watch for changes from NestJS
  receiveChange(curr1: string, curr2: string){
    return this.socket.fromEvent('crypto-changes-' + curr1 + '-' + curr2);
  }
}
