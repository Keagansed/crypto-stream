import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponentComponent } from './framework/homepage-component/homepage-component.component';


const routes: Routes = [
  {
    path: 'home',
    component: HomepageComponentComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'home'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
