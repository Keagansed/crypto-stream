import { Component, OnInit } from '@angular/core';
import { CryptoService } from 'src/app/services/crypto.service';
import { Chart } from 'angular-highcharts';
import { style } from '@angular/animations';
import { Card } from 'src/app/models/card.model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-homepage-component',
  templateUrl: './homepage-component.component.html',
  styleUrls: ['./homepage-component.component.scss']
})
export class HomepageComponentComponent {
  cards: Array<Card> = []
  private subscriptions: Array<{fromCurr: string, toCurr: string, subscription: Subscription}> = []
  cryptoExchange = []
  count: number = 0

  constructor(private cryptoService: CryptoService, public dialog: MatDialog, private _snackBar: MatSnackBar) { }

  openDialog() {
      const dialogRef = this.dialog.open(AddCardDialog);
      dialogRef.afterClosed().subscribe(result => {
        if (result.fromCurr && result.toCurr) {
          this.subscribe(result.fromCurr, result.toCurr)
        } else {
          this.openSnackBar('You have not made a selection, please try again.', 'Close')
        }
      });
    }

  // Removes card from the screen and unsubscribes from connections
  removeCard(fromCurr: string, toCurr: string): void {
    this.subscriptions.forEach((subscription) => {
      subscription.subscription.unsubscribe()
    })
    console.log(this.subscriptions.filter((subscription) => { return (subscription.fromCurr !== fromCurr || subscription.toCurr !== toCurr)}))
    this.subscriptions = this.subscriptions.filter((subscription) => { return (subscription.fromCurr !== fromCurr || subscription.toCurr !== toCurr)})
    this.cards = this.cards.filter((cards) => { return (cards.fromCurr !== fromCurr || cards.toCurr !== toCurr)})
    this.cryptoService.unsubscribe(fromCurr,toCurr)
    this.count--
  }

  // Creates a new card and subscribes to exchange pair using NestJS backend
  subscribe(fromCurr: string, toCurr: string): void {
    if (toCurr === 'JPY' || toCurr === 'ZAR') {
      this.openSnackBar('The currency ' + toCurr + ' is not supported. Please select another and try again.', 'Close')
    } else {
      let found = false
      this.subscriptions.map((subscription) => {
          if (subscription.fromCurr === fromCurr && subscription.toCurr === toCurr)
              found = true
      })
      let cardNumber = this.count++
      if (!found) { // This card doesn't already exist
        this.cards.push({
          fromCurr: fromCurr,
          toCurr: toCurr,
          exchangeRate: undefined,
          chart: new Chart({
            chart: {
              type: 'line',
              height: 220,
              backgroundColor: 'rgba(255,255,255,0.1)',
            },
            title: {
              text: 'History',
              style: {'color': 'white'}
            },
            xAxis: {
              gridLineColor: 'white',
              title: {
                style: {'color': 'white'}
              },
              labels: {
                style: {'color': 'white'}
              }
            },
            yAxis: {
              gridLineColor: 'white',
              title: {
                style: {'color': 'white'}
              },
              labels: {
                style: {'color': 'white'}
              }
            },
            colors: ['#f9a52d'],
            credits: {
              enabled: false
            },
            legend: {
              itemStyle: {
                color: '#ffffff'
              }
            },
            series: [
              {  
                name: 'Value',
                type: 'line',
                data: [],
              }
            ]
          })
        })
  
        this.cryptoService.subscribe(fromCurr, toCurr)
        let sub =  this.cryptoService.receiveChange(fromCurr, toCurr).subscribe((message: string) => {
          if (JSON.parse(message).PRICE  !== undefined){
            this.cards[cardNumber].exchangeRate = JSON.parse(message).PRICE
            this.cards[cardNumber].chart.addPoint(JSON.parse(message).PRICE)
          }
        })
        this.subscriptions.push({fromCurr: fromCurr,toCurr: toCurr, subscription: sub})
      } else { // This card does exist already
        this.openSnackBar('This currency pair is already being displayed.', 'Close')
      }
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}

@Component({
  selector: 'add-card-dialog',
  templateUrl: 'add-card-dialog.html',
})
export class AddCardDialog {
  data: {fromCurr: string, toCurr: string} | boolean = {fromCurr: '', toCurr: ''}
}