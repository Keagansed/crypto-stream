# Crypto Stream Angular
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.

## Installation
Install the node modules:
```bash
npm install
```
Start the Angular DEV server:
```bash
npm run start
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
r/angular-cli/blob/master/README.md).
